
Page({

  STUDENT_ID(e){
    console.log(e.detail.value);
    this.setData({
      STUDENT_ID:e.detail.value
    })
  },

  STUDENT_NAME(e){
    console.log(e.detail.value);
    this.setData({
      STUDENT_NAME:e.detail.value
    })
  },


  //输入新密码
  PASSWORD1(e){
    console.log(e.detail.value);
    this.setData({
      password1:e.detail.value
    })
  },
  //输入确认密码
  PASSWORD2(e){
    console.log(e.detail.value);
    this.setData({
      password2:e.detail.value
    })
    if(this.data.password1==this.data.password2)
      {console.log("hege");
      this.setData({
        button_a:false
      })  
    }

  },

  change_password(){
    wx.request({
      url: 'https://www.campustransaction.xyz/A_M/C_P/',
      data:{
        USER_ID:this.data.user_id,
        STUDENT_ID:this.data.STUDENT_ID,
        STUDENT_NAME:this.data.STUDENT_NAME,
        USER_PASSWORD:this.data.password1,
      },
      success(res){
        console.log(res)
        if(res.data[0].result=="成功！")
        {
          
          wx.showLoading({
            title: '成功',
            mask:true
          })
          setTimeout(function () {
            wx.hideLoading()
          }, 1000)
        }
        else{
          wx.showLoading({
            title: '失败',
            mask:true
          })
          setTimeout(function () {
            wx.hideLoading()
          }, 1000)
        }
        wx.reLaunch({
          url: '../my/my',
        })
      }
    })
  },
  /**
   * 页面的初始数据
   */
  data: {
    user_id:"",
    STUDENT_ID:"",
    STUDENT_NAME:"",
    password1:"",
    password2:"",
    button_a:true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that=this
    wx.getStorage({
      key: 'user_id',
      success (res) {
        console.log(res.data)
        that.setData({
          user_id:res.data,
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})