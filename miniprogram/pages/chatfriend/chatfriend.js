// pages/chatfriend/chatfriend.js
//云数据库初始化
const db = wx.cloud.database({});
const cont = db.collection('river_data');

Page({
   data:{
        ne:[],  //这是一个空的数组，等下获取到云数据库的数据将存放在其中
        user_id:"",
        groupId_avatar:[],
        groupId_avatar2:[],
        xunwenflog1:false,//买家询问
        xunwenflog2:false,//我的询问
        xunwen1:"",
        xunwen2:"",
    },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _this = this;
    var flag=0
    var oneMchat=0
    var oneMchat2=0
    var oneWchat=0
    var oneWchat2=0
    var that=this
    //1、引用数据库   
    const db = wx.cloud.database({
      //这个是环境ID不是环境名称     
      env: 'xiaoyi'
    })
    //2、开始查询数据了  news对应的是集合的名称   
    db.collection('chatroom').get({
      //如果查询成功的话    
      success: res => {
        console.log(res.data)
        //这一步很重要，给ne赋值，没有这一步的话，前台就不会显示值      
        this.setData({
          ne: res.data
        })
        console.log(that.data.ne)
        //得到有自己id的聊天室 
        for(var i=this.data.ne.length-1;i>=0;i--){
          //买家的询问
          if(this.data.ne[i].groupId.split("%")[0]==this.data.user_id&&this.data.ne[i].user_id!=this.data.user_id){
            var obj={}
            obj.groupid=that.data.ne[i].groupId
            obj.avatar=that.data.ne[i].avatar
            obj.nickName=that.data.ne[i].nickName
            if(flag==0){
              if(oneMchat==0){
              that.setData({
                //groupId_avatar:[...this.data.groupId_avatar,this.data.ne[i].groupId]
                groupId_avatar2:[...that.data.groupId_avatar2,obj]
              })
              oneMchat=1
            }
            for(var t in that.data.groupId_avatar2){
              if(that.data.groupId_avatar2[t].groupid==that.data.ne[i].groupId){
                oneMchat2=1
              }
            }
            if(oneMchat2==0){
                that.setData({
                  //groupId_avatar:[...this.data.groupId_avatar,this.data.ne[i].groupId]
                  groupId_avatar:[...that.data.groupId_avatar2,obj]
                })
            }
              if(that.data.groupId_avatar2.length!=0){
                that.setData({
                  xunwen1:"买家的询问",
                  xunwenflog1:true,
                })
              }
              oneMchat2=0
            }
            // elif()
          }
          //我的询问
          if(this.data.ne[i].groupId.split("%")[1]==this.data.user_id&&this.data.ne[i].user_id!=this.data.user_id){
            var obj={}
            obj.groupid=that.data.ne[i].groupId
            obj.avatar=that.data.ne[i].avatar
            obj.nickName=that.data.ne[i].nickName
            if(flag==0){
              if(oneWchat==0){
                that.setData({
                  //groupId_avatar:[...this.data.groupId_avatar,this.data.ne[i].groupId]
                  groupId_avatar:[...that.data.groupId_avatar,obj]
                })
                oneWchat=1
              }
              for(var t in that.data.groupId_avatar){
                if(that.data.groupId_avatar[t].groupid==that.data.ne[i].groupId){
                  oneWchat2=1
                }
              }
              if(oneWchat2==0){
                  that.setData({
                    //groupId_avatar:[...this.data.groupId_avatar,this.data.ne[i].groupId]
                    groupId_avatar:[...that.data.groupId_avatar,obj]
                  })
              }
              if(that.data.groupId_avatar.length!=0){
                that.setData({
                  xunwen2:"我的询问",
                  xunwenflog2:true,
                })
              }
              oneWchat2=0
            }
            // elif()
          }
        }
      }
    })
    wx.getStorage({
      key: 'user_id',
      success (res) {
        console.log(res.data)
        _this.setData({
          user_id:res.data,
        })
      }
    })


  },    

  refreshchat(){
    let that=this
    var flag=0
    var oneMchat=0
    var oneMchat2=0
    var oneWchat=0
    var oneWchat2=0
    //聊天室清空
    this.setData({
      groupId_avatar:[],
      groupId_avatar2:[],
    })
    //得到有自己id的聊天室  
    for(var i=this.data.ne.length-1;i>=0;i--){
      //买家的询问
      if(this.data.ne[i].groupId.split("%")[0]==this.data.user_id&&this.data.ne[i].user_id!=this.data.user_id){
        var obj={}
        obj.groupid=that.data.ne[i].groupId
        obj.avatar=that.data.ne[i].avatar
        obj.nickName=that.data.ne[i].nickName
        if(flag==0){
          if(oneMchat==0){
          that.setData({
            //groupId_avatar:[...this.data.groupId_avatar,this.data.ne[i].groupId]
            groupId_avatar2:[...that.data.groupId_avatar2,obj]
          })
          oneMchat=1
        }
        for(var t in that.data.groupId_avatar2){
          if(that.data.groupId_avatar2[t].groupid==that.data.ne[i].groupId){
            oneMchat2=1
          }
        }
        if(oneMchat2==0){
            that.setData({
              //groupId_avatar:[...this.data.groupId_avatar,this.data.ne[i].groupId]
              groupId_avatar:[...that.data.groupId_avatar2,obj]
            })
        }
          if(that.data.groupId_avatar2.length!=0){
            that.setData({
              xunwen1:"买家的询问",
              xunwenflog1:true,
            })
          }
          oneMchat2=0
        }
        // elif()
      }
      //我的询问
      if(this.data.ne[i].groupId.split("%")[1]==this.data.user_id&&this.data.ne[i].user_id!=this.data.user_id){
        var obj={}
        obj.groupid=that.data.ne[i].groupId
        obj.avatar=that.data.ne[i].avatar
        obj.nickName=that.data.ne[i].nickName
        if(flag==0){
          if(oneWchat==0){
            that.setData({
              //groupId_avatar:[...this.data.groupId_avatar,this.data.ne[i].groupId]
              groupId_avatar:[...that.data.groupId_avatar,obj]
            })
            oneWchat=1
          }
          for(var t in that.data.groupId_avatar){
            if(that.data.groupId_avatar[t].groupid==that.data.ne[i].groupId){
              oneWchat2=1
            }
          }
          if(oneWchat2==0){
              that.setData({
                //groupId_avatar:[...this.data.groupId_avatar,this.data.ne[i].groupId]
                groupId_avatar:[...that.data.groupId_avatar,obj]
              })
          }
          if(that.data.groupId_avatar.length!=0){
            that.setData({
              xunwen2:"我的询问",
              xunwenflog2:true,
            })
          }
          oneWchat2=0
        }
        // elif()
      }
    }
  },

  go(e){
       wx.navigateTo({
        url: '/pages/room/room?id=' + e.currentTarget.dataset.id,
       })
  }

})