import {request} from "../../request/index.js";
Page({
  async getGoodslist(){
    let that=this
    const pagination=this.QueryParams2.pagination
    const capacity=this.QueryParams2.capacity
    const Commodity_name=this.QueryParams2.Commodity_name
    console.log(pagination);
    console.log(capacity);
    console.log(Commodity_name);
    
    wx.request({
      url: 'https://www.campustransaction.xyz/Q_M/H_P_Q/',
      data:{
        pagination:pagination,
        capacity:capacity,
        Commodity_name:Commodity_name,
      },
      success(res){
        // that.QueryParams2.pagination=Math.ceil(res.data[0].total/that.QueryParams2.capacity)
        // console.log(that.QueryParams2.pagination)
        that.setData({
          goodslist:res.data,
        })
        console.log(res)
        if(res.data[0].result=="没找到商品！"){
          
        }
        else{
        that.setData({
          // goodslist2:res.data[2].goods,
          goodslist2:[...that.data.goodslist2,...res.data[2].goods],
        })
      }
        
        
      }
    })
    
    
  },
  QueryParams2:{
    pagination:1,
    capacity:4,
    Commodity_name:"",
    index:"",
  },
  /**
   * 页面的初始数据
   */
  data: {

    goodslist2:[],
    swiperList:[
      "cloud://xiaoyi.7869-xiaoyi-1301977723/lunbo/lunbo1.png",
      "cloud://xiaoyi.7869-xiaoyi-1301977723/lunbo/lunbo2.png",
      "cloud://xiaoyi.7869-xiaoyi-1301977723/lunbo/lunbo3.png",
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getGoodslist()
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})