// pages/search/search.js
Page({


  emptyHistory(){
    this.setData({
      searchHistory:""
    })
    wx.setStorage({
      key:"search_history",
      data:[]
    })
  },
  /**
   * 页面的初始数据
   */
  search(){
    console.log(this.data.searchValue)
  },
  //接收输入框数据
  searchInput(e){
    this.setData({
      searchValue:e.detail.value
    })
  },
  //点击热门搜索里的属性时触发
  
  hotSearch(e){
    this.setData({
      searchValue:e.target.dataset.option,
      input_value:e.target.dataset.option
    })
    this.search()
  },
  
  data: {
    searchHistory:[
      
    ],
    searchValue:"",
    input_value:"",
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that=this
    wx.getStorage({
      key: 'search_history',
      success (res) {
        console.log(res.data)
        that.data.searchHistory=that.data.searchHistory.concat(res.data)
        that.setData({
          searchHistory:that.data.searchHistory
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})